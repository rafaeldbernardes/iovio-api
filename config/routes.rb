# == Route Map
#
#            Prefix Verb   URI Pattern                            Controller#Action
# api_user_projects GET    /api/users/:user_id/projects(.:format) api/projects#index
#         api_users GET    /api/users(.:format)                   api/users#index
#                   POST   /api/users(.:format)                   api/users#create
#          api_user GET    /api/users/:id(.:format)               api/users#show
#                   PATCH  /api/users/:id(.:format)               api/users#update
#                   PUT    /api/users/:id(.:format)               api/users#update
#                   DELETE /api/users/:id(.:format)               api/users#destroy
#      api_projects GET    /api/projects(.:format)                api/projects#index
#                   POST   /api/projects(.:format)                api/projects#create
# 

Rails.application.routes.draw do
  namespace :api do
    resources :users do
      resources :projects, only: [:index]
    end

    resources :projects, only: [:create, :index]
  end
end
