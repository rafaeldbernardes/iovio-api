## Requirements
  * Ruby 2.4.1p111
  * Rails 5.1.4

## Routes
All of the routes have been tested with Postman.

```rb
# == Route Map
#
#            Prefix Verb   URI Pattern                            Controller#Action
# api_user_projects GET    /api/users/:user_id/projects(.:format) api/projects#index
#         api_users GET    /api/users(.:format)                   api/users#index
#                   POST   /api/users(.:format)                   api/users#create
#          api_user GET    /api/users/:id(.:format)               api/users#show
#                   PATCH  /api/users/:id(.:format)               api/users#update
#                   PUT    /api/users/:id(.:format)               api/users#update
#                   DELETE /api/users/:id(.:format)               api/users#destroy
#      api_projects GET    /api/projects(.:format)                api/projects#index
#                   POST   /api/projects(.:format)                api/projects#create
# 
```

## API's Provided by IOVIO
I made a draw to show which API's was provided and to clarify the system.

![alt text](https://i.imgur.com/cWDjWKM.png)

## Installation Instructions

The project will serve in port 3000.

To run the unit and integration tests, run the following:
```sh
$ API_ENDPOINT_URL="http://localhost:5000" rspec
```

* Integration tests inside folder spec/integrations should be runned with qualibrate-api up and running.

API_ENDPOINT_URL = Environment variable, it's been used with command inline, can be set to whole environment as well.

### Windows 64 Bits

Download http://rubyinstaller.org/downloads/:
  * DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe
  * http://chromedriver.storage.googleapis.com/index.html?path=2.9/

Install Ruby, and add to path:
  * <pasta devkit>\bin
  * <pasta devkit>\mingw\bin
  * <pasta chrome driver>chrome_drive_folder

### Linux 64 Bits

Installing Ruby on Ubuntu:
  * [RVM](https://rvm.io/)

## Up and running

Inside folder's project, run:
```sh
$ gem install bundler
$ bundle install (could take a while)
$ API_ENDPOINT_URL="http://localhost:5000" rails s
```
