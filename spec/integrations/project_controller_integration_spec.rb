require 'spec_helper'

RSpec.describe ProjectService, type: :service do
  before(:each) do
    WebMock.allow_net_connect!
  end

  let(:service) { described_class.new(parameters) }

  describe 'Projects load all' do
    context 'with valid data' do
      it 'should return 200 code' do
        response = Project.all

        expect(response._status).to eq(200)
      end
    end
  end
end