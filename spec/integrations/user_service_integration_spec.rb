require 'spec_helper'

RSpec.describe UserService, type: :service do
  before(:each) do
    WebMock.allow_net_connect!
  end

  let(:service) { described_class.new(parameters) }

  describe 'User creation' do
    context 'with valid data' do
      let(:parameters) {{ 'first_name' => 'Foo', 'last_name' => 'Bar', 'email' => Faker::Internet.email }}

      it 'should return success true' do
        service.create

        expect(service.success).to eq(true)
      end

      it 'should return the record with the data' do
        service.create

        user = service.record

        expect(user.first_name).to eq(parameters['first_name'])
        expect(user.last_name).to eq(parameters['last_name'])
        expect(user.email).to eq(parameters['email'])
      end
    end
  end

  describe 'User updating' do
    let(:parameters) {{ 'first_name' => 'Laa', 'last_name' => 'Land', 'email' => Faker::Internet.email }}

    before(:each) do
      @user = service.create
    end

    context 'with valid data' do
      it 'should return success true' do
        service.update(@user.id)
        
        expect(service.success).to eq(true)
      end

      it 'should return the record with the data' do
        service.update(@user.id)

        user = service.record

        expect(user.first_name).to eq(parameters['first_name'])
        expect(user.last_name).to eq(parameters['last_name'])
        expect(user.email).to eq(parameters['email'])
      end
    end
  end

  describe 'User destroying' do
    let(:parameters) {{ 'first_name' => 'Laa', 'last_name' => 'Land', 'email' => Faker::Internet.email }}

    before(:each) do
      @user = service.create
    end

    context 'with valid data' do
      it 'should return success true' do
        service.destroy(@user.id)

        expect(service.success).to eq(true)
      end
    end
  end
end