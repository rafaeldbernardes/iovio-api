require 'spec_helper'

RSpec.describe UserService, type: :service do
  before(:each) do
    WebMock.disable_net_connect!
    @user = FactoryBot.create(:user)
  end

  let(:service) { described_class.new(parameters) }

  describe 'user creation' do
    before(:each) do
      WebMock.reset!
      @user = FactoryBot.create(:user)
      allow_any_instance_of(User).to receive(:create).and_return(@user)
      allow_any_instance_of(User).to receive(:find).and_return(@user)

      stub_request(:post, ENV.fetch("API_ENDPOINT_URL") + "/users")
        .to_return(body: "{'first_name': 'Bar', 'last_name': 'Foo', 'email': 'test@email'}", status: 200)
      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/").to_return(body: "{}", status: 200)
    end

    context 'with invalid data' do
      let(:parameters) {{ 'first_name' => '', 'last_name' => 'Foo', 'email' => 'test@email' }}

      before(:each) do
        allow_any_instance_of(User).to receive(:create).and_return(false)
      end

      it 'should return false' do
        service.create

        expect(service.success).to eq(false)
      end

      it 'should return the errors' do
        service.create

        expect(service.errors).to_not be_empty
        expect(service.errors[0]).to eq("First name can't be blank")
      end
    end

    context 'with valid data' do
      let(:parameters) {{ 'first_name' => 'Foo', 'last_name' => 'Bar', 'email' => 'test@email.com' }}

      it 'should return true' do
        service.create

        expect(service.success).to eq(true)
      end

      it 'should not return the errors' do
        service.create

        expect(service.errors).to eq(nil)
      end
    end
  end

  describe 'user updating' do
    before(:each) do
      WebMock.reset!
      @user = FactoryBot.build(:user, id: 1)

      allow_any_instance_of(User).to receive(:save).and_return(@user)
      allow_any_instance_of(User).to receive(:find).and_return(@user)

      stub_request(:put, ENV.fetch("API_ENDPOINT_URL") + "/users/#{@user.id}")
        .to_return(body: "{'first_name': 'Bar', 'last_name': 'Foo', 'email': 'test@email'}", status: 200)
      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/#{@user.id}").to_return(body: "{}", status: 200)
    end

    context 'with invalid data' do
      let(:parameters) {{ 'first_name' => '', 'last_name' => 'Foo', 'email' => 'test@email' }}

      before(:each) do
        allow_any_instance_of(User).to receive(:save).and_return(false)
      end

      it 'should return false' do
        service.update(@user.id)
        
        expect(service.success).to eq(false)
      end

      it 'should return the errors' do
        service.update(@user.id)
        
        expect(service.errors).to_not be_empty
      end
    end
  end

  describe 'user destroying' do
    let(:service) { described_class.new }

    before(:each) do
      WebMock.reset!
      @user = FactoryBot.build(:user, id: 1)

      allow_any_instance_of(User).to receive(:remove).and_return(@user)
      allow_any_instance_of(User).to receive(:find).and_return(@user)

      stub_request(:delete, ENV.fetch("API_ENDPOINT_URL") + "/users/#{@user.id}").to_return(body: "", status: 200)
      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/#{@user.id}").to_return(body: "{}", status: 200)
    end

    context 'with invalid data' do
      before(:each) do
        allow_any_instance_of(User).to receive(:remove).and_return(false)
      end

      it 'should return false' do
        service.destroy(@user.id)
        
        expect(service.success).to eq(false)
      end
    end
  end
end