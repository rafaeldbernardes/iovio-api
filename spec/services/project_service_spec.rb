require 'spec_helper'

RSpec.describe ProjectService, type: :service do
  before(:each) do
    WebMock.disable_net_connect!
    @project = FactoryBot.create(:project)
  end

  let(:service) { described_class.new(parameters) }

  describe 'project creation' do
    before(:each) do
      WebMock.reset!
      @project = FactoryBot.create(:project)
      allow_any_instance_of(Project).to receive(:create).and_return(@project)
      allow_any_instance_of(Project).to receive(:find).and_return(@project)

      stub_request(:post, ENV.fetch("API_ENDPOINT_URL") + "/projects")
        .to_return(body: "{'name': 'Lars', 'code': '51651561', 'description': 'Lorem ..', 'user_id' => '2'}", status: 200)
    end

    context 'with invalid data' do
      let(:parameters) {{ 'name' => '', 'code' => '51651561', 'description' => 'Lorem ..', 'user_id' => '2' }}

      before(:each) do
        allow_any_instance_of(Project).to receive(:create).and_return(false)
      end

      it 'should return false' do
        service.create

        expect(service.success).to eq(false)
      end

      it 'should return the errors' do
        service.create

        expect(service.errors).to_not be_empty
        expect(service.errors[0]).to eq("Name can't be blank")
      end
    end

    context 'with valid data' do
      let(:parameters) {{ 'name' => 'Foo', 'code' => '51651561', 'description' => 'Lorem ...', 'user_id' => '2' }}

      it 'should return true' do
        service.create

        expect(service.success).to eq(true)
      end

      it 'should not return the errors' do
        service.create

        expect(service.errors).to eq(nil)
      end
    end
  end
end