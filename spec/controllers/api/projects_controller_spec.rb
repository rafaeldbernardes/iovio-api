require 'spec_helper'

RSpec.describe Api::ProjectsController, type: :controller do
  describe 'GET index' do
    before(:each) do
      WebMock.disable_net_connect!
      @project = FactoryBot.create(:project)
      allow(Project).to receive(:all).and_return([@project])
    end

    context 'json request' do
      it 'should return ok status' do
        get :index, xhr: true

        expect(response).to have_http_status(:ok)
      end

      it 'should return the projects' do
        get :index, xhr: true

        expect(json.size).to eq(1)

        expect(json[0]["name"]).to eq(@project.name)
        expect(json[0]["code"]).to eq(@project.code)
        expect(json[0]["description"]).to eq(@project.description)
      end
    end
  end

  describe 'POST create' do
    before(:each) do
      WebMock.disable_net_connect!
      @project = FactoryBot.create(:project)
      allow(ProjectService).to receive(:new).and_return(project_service)
    end

    context 'with invalid parameters' do
      let(:project_service) { double(:project_service, create: false, success: false, errors: ['foo']) }
      let(:parameters) {{ 'name' => '', 'code' => '561dasdas', 'description' => 'Lorem ipslum' }}

      it 'should return the unprocessable entity status' do
        post :create, params: parameters, xhr: true

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the errors' do
        post :create, params: parameters, xhr: true

        expect_json_types(errors: :array_of_strings)
      end
    end

    context 'with valid parameters' do
      let(:project_service) { double(:project_service, create: true, success: true, record: @project, errors: []) }
      let(:parameters) {{ 'name' => 'Lars', 'code' => '561dasdas', 'description' => 'Lorem ipslum' }}

      it 'should return the created status' do
        post :create, params: parameters, xhr: true

        expect(response).to have_http_status(:created)
      end
    end
  end
end