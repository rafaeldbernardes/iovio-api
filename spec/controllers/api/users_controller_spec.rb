require 'spec_helper'

RSpec.describe Api::UsersController, type: :controller do
  describe 'GET index' do
    before(:each) do
      WebMock.disable_net_connect!
      @user = FactoryBot.create(:user)
      allow(User).to receive(:all).and_return([@user])
    end

    context 'json request' do
      it 'should return ok status' do
        get :index, xhr: true

        expect(response).to have_http_status(:ok)
      end

      it 'should return the users' do
        get :index, xhr: true

        expect(json.size).to eq(1)

        expect(json[0]["first_name"]).to eq(@user.first_name)
        expect(json[0]["last_name"]).to eq(@user.last_name)
        expect(json[0]["email"]).to eq(@user.email)
      end
    end
  end

  describe 'POST create' do
    before(:each) do
      WebMock.disable_net_connect!
      @user = FactoryBot.create(:user)
      allow(UserService).to receive(:new).and_return(user_service)
    end

    context 'with invalid parameters' do
      let(:user_service) { double(:user_service, create: false, success: false, errors: ['foo']) }
      let(:parameters) {{ 'first_name' => '', 'last_name' => 'Foo', 'email' => 'lars@test.com' }}

      it 'should return the unprocessable entity status' do
        post :create, params: parameters, xhr: true

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the errors' do
        post :create, params: parameters, xhr: true

        expect_json_types(errors: :array_of_strings)
      end
    end

    context 'with valid parameters' do
      let(:user_service) { double(:user_service, create: true, success: true, record: @user, errors: []) }
      let(:parameters) {{ 'first_name' => 'Lars', 'last_name' => 'Foo', 'email' => 'lars@test.com' }}

      it 'should return the created status' do
        post :create, params: parameters, xhr: true

        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'PUT update' do
    before(:each) do
      WebMock.disable_net_connect!
      @user = FactoryBot.create(:user, id: 1)

      allow(User).to receive(:find).and_return(@user)
      allow(UserService).to receive(:new).and_return(user_service)

      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/").to_return(body: "{}", status: 200)
    end

    context 'with invalid parameters' do
      let(:user_service) { double(:user_service, update: false, success: false, errors: ['foo']) }
      let(:parameters) {{ 'first_name' => '', 'last_name' => 'Foo', 'email' => 'lars@test.com' }}

      it 'should return the unprocessable entity status' do
        put :update, params: parameters.merge(id: @user.id), xhr: true

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should return the errors' do
        put :update, params: parameters.merge(id: @user.id), xhr: true

        expect_json_types(errors: :array_of_strings)
      end
    end

    context 'with valid parameters' do
      let(:user_service) { double(:user_service, update: true, success: true, errors: []) }
      let(:parameters) {{ 'first_name' => 'Lars', 'last_name' => 'Foo', 'email' => 'lars@test.com' }}

      it 'should return the ok status' do
        put :update, params: parameters.merge(id: @user.id), xhr: true

        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET show' do
    before(:each) do
      WebMock.disable_net_connect!
      @user = FactoryBot.create(:user, id: 1)

      allow(User).to receive(:find).and_return(@user)

      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/").to_return(body: "{}", status: 200)
    end

    it 'should return ok status' do
      get :show, params: {id: @user.id}, xhr: true

      expect(response).to have_http_status(:ok)
    end

    it 'should return the json structure' do
      get :show, params: {id: @user.id}, xhr: true

      expect_json_types(first_name: :string, last_name: :string, email: :string)
    end

    it 'should return the user data' do
      get :show, params: {id: @user.id}, xhr: true

      expect_json(first_name: @user.first_name, last_name: @user.last_name, email: @user.email)
    end
  end

  describe 'DELETE destroy' do
    before(:each) do
      WebMock.disable_net_connect!
      @user = FactoryBot.create(:user, id: 1)

      allow(User).to receive(:find).and_return(@user)
      allow(UserService).to receive(:new).and_return(user_service)

      stub_request(:get, ENV.fetch("API_ENDPOINT_URL") + "/users/").to_return(body: "{}", status: 200)
    end

    context 'with error' do
      let(:user_service) { double(:user_service, destroy: false, success: false, errors: ['foo']) }

      it 'should return bad request status' do
        delete :destroy, params: { id: @user.id }, xhr: true

        expect(response).to have_http_status(:bad_request)
      end

      it 'should return the the errors' do
        delete :destroy, params: { id: @user.id }, xhr: true

        expect_json_types(errors: :array_of_strings)
      end
    end

    context 'with success' do
      let(:user_service) { double(:user_service, destroy: true, success: true, errors: []) }

      it 'should return no content status' do
        delete :destroy, params: { id: @user.id }, xhr: true

        expect(response).to have_http_status(:no_content)
      end
    end
  end
end