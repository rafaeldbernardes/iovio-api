FactoryBot.define do
  factory :project do
    name          { Faker::Name.name }
    user_id       { Faker::Number.between(1,2) }
    code          { Faker::Code.isbn }
    description   { Faker::Lorem.sentence }
  end
end