require 'spec_helper'

RSpec.describe Project, type: :model do
  before(:each) do
    @user = FactoryBot.create(:user)
  end

  context "validation" do
    it "should invalidate when the name is not present" do
      expect(FactoryBot.build(:project, name: nil)).to be_invalid
    end

    it "should invalidate when the name is blank" do
      expect(FactoryBot.build(:project, name: "")).to be_invalid
    end

    it "should invalidate when the user id not present" do
      expect(FactoryBot.build(:project, user_id: nil)).to be_invalid
    end

    it "should invalidate when the user id is blank" do
      expect(FactoryBot.build(:project, user_id: "")).to be_invalid
    end

    it "should pass" do
      expect(FactoryBot.build(:project)).to be_valid
    end
  end
end