require 'spec_helper'

RSpec.describe User, type: :model do
  context "validation" do
    it "should invalidate when the first name is not present" do
      expect(FactoryBot.build(:user, first_name: nil)).to be_invalid
    end

    it "should invalidate when the first name is blank" do
      expect(FactoryBot.build(:user, first_name: "")).to be_invalid
    end

    it "should invalidate when the last name is not present" do
      expect(FactoryBot.build(:user, last_name: nil)).to be_invalid
    end

    it "should invalidate when the last name is blank" do
      expect(FactoryBot.build(:user, last_name: "")).to be_invalid
    end

    it "should invalidate when the email is not present" do
      expect(FactoryBot.build(:user, email: nil)).to be_invalid
    end

    it "should invalidate when the email is blank" do
      expect(FactoryBot.build(:user, email: "")).to be_invalid
    end

    it "should pass" do
      expect(FactoryBot.build(:user)).to be_valid
    end
  end
end