class ProjectService
  attr_reader :success, :errors

  def initialize(parameters = {})
    @parameters = parameters
  end

  def create
    save_record(Project.new(@parameters))
  end

  private
    def save_record(project)
      if project.valid? && project.create
        @success = true
      else
        @success = false
        @errors = project.errors.full_messages
      end
    end
end