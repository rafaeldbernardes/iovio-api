class UserService
  attr_reader :success, :errors, :record

  def initialize(parameters = {})
    @parameters = parameters
  end

  def create
    save_record(User.new(@parameters))
  end

  def update(user_id)
    user = find_user(user_id)

    user.first_name = @parameters["first_name"]
    user.last_name = @parameters["last_name"]
    user.email = @parameters["email"]

    update_record(user)
  end

  def destroy(user_id)
    user = find_user(user_id)

    if user.remove
      @success = true
    else
      @success = false
      @errors = user.errors.full_messages
    end
  end

  private
    def save_record(user)
      if user.valid? && user.create
        @success = true
        @record = find_user(user.id)
      else
        @success = false
        @errors = user.errors.full_messages
      end
    end

    def update_record(user)
      if user.valid? && user.save
        @success = true
        @record = user
      else
        @success = false
        @errors = user.errors.full_messages
      end
    end

    def find_user(user_id)
      User.find(user_id)
    end
end