module Api
  class UsersController < ApplicationController
    before_action :set_user, only: [:show, :update, :destroy]

    def index
      @users = User.all
      render json: @users
    end

    def show
      render json: @user.to_json, status: :ok
    end

    def create
      service = UserService.new(user_params)

      service.create

      if service.success
        @user = service.record

        render nothing: true, status: :created
      else
        render json: { errors: service.errors }, status: :unprocessable_entity
      end
    end

    def update
      service = UserService.new(user_params)

      service.update(params[:id])

      if service.success
        render nothing: true, status: :ok
      else
        render json: { errors: service.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      service = UserService.new

      service.destroy(params[:id])

      if service.success
        render nothing: true, status: :no_content
      else
        render json: { errors: service.errors }, status: :bad_request
      end
    end

    private
      def set_user
        @user = User.find(params[:id])
      end

      def user_params
        params.permit(:title, :first_name, :last_name, :email)
      end
  end
end