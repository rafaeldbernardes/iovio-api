module Api
  class ProjectsController < ApplicationController
    def index
      @projects = Project.all
      render json: @projects
    end

    def create
      service = ProjectService.new(project_params)

      service.create

      if service.success
        render nothing: true, status: :created
      else
        render json: { errors: service.errors }, status: :unprocessable_entity
      end
    end

    private
      def project_params
        params.permit(:name, :code, :description, :user_id)
      end
  end
end