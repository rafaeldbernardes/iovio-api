class Project < Flexirest::Base
	extend ActiveModel::Naming
  include ActiveModel::Conversion
	include ActiveModel::Validations
	
	before_request :override_default_content_type
	before_request :replace_body
  
  validates :user_id, presence: true
	validates :name, presence: true

	get :all, "/projects"
	post :create, "/projects"
	get :find, "/users/:user_id/projects"

	private
		def override_default_content_type(name, request)
			if name == :save || name == :create
				request.headers["Content-Type"] = "application/json"
			end
		end

		def replace_body(name, request)
			if name == :create || name == :save
				request.body = request.post_params.to_json
			end
		end
end