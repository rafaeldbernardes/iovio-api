class User < Flexirest::Base
	extend ActiveModel::Naming
  include ActiveModel::Conversion
	include ActiveModel::Validations

	before_request :override_default_content_type
	before_request :replace_body

	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :email, presence: true

	get :all, "/users"
	get :find, "/users/:id"
	put :save, "/users/:id"
	post :create, "/users"
	delete :remove, "/users/:id"

	private
		def override_default_content_type(name, request)
			if name == :save || name == :create
				request.headers["Content-Type"] = "application/json"
			end
		end

		def replace_body(name, request)
			if name == :create || name == :save
				request.body = request.post_params.to_json
			end
		end
end